'use strict';

angular.module('surepulseDevTestApp')
    .controller('AuthCtrl', function($auth, $route, $scope, $location, Config, $http, $rootScope) {
        var baseUrl = Config.apiBase  + '/authenticate/user';
        $scope.loginData = {
            email   : '',
            password: ''
        };

        $rootScope.login = function() {
            var credentials = {
                email   : $scope.loginData.email,
                password: $scope.loginData.password
            };

            $auth.login(credentials).then(function() {

                return $http({
                    method: 'GET',
                    url: baseUrl,
                    headers: {
                        Authorization: 'Bearer ' + $auth.getToken()
                    }
                }).then(function (response) {
                    var user = response.data.user;

                    localStorage.setItem('user', JSON.stringify(response.data.user));

                    $rootScope.authenticated = true;

                    $rootScope.currentUser = response.data.user;
                    $location.path('/');
                });

            }, function(error) {
              $scope.error = error.data.message;
            });
      };
    })
    .run(function ($rootScope, $auth, $location) {
        var user = window.localStorage.getItem('user');
        if (user) {
          $rootScope.currentUser = JSON.parse(user);
          $rootScope.authenticated = true;
        } else {
          $rootScope.currentUser = null;
          $rootScope.authenticated = false;
        }
        $rootScope.logout = function() {
            $auth.logout().then(function() {
                localStorage.removeItem('user');

                $rootScope.authenticated = false;

                $rootScope.currentUser = null;
                $location.path('/login');
            });
        };
    });
