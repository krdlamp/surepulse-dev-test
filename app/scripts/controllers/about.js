'use strict';

/**
 * @ngdoc function
 * @name surepulseDevTestApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the surepulseDevTestApp
 */
angular.module('surepulseDevTestApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
