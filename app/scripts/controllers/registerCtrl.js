'use strict';

angular.module('surepulseDevTestApp')
  .controller('RegisterCtrl', function($route, $auth, $scope, $rootScope, Config, $http, $location) {

    $scope.register = function() {
      if($scope.user.password === $scope.user.password_confirmation) {
        $auth.signup({
          first_name              : $scope.user.first_name,
          last_name               : $scope.user.last_name,
          email                   : $scope.user.email,
          password                : $scope.user.password,
          password_confirmation   : $scope.user.password_confirmation
        }).then(function (response) {
          window.alert("You've been successfully registered!")
          $location.path('/login');

        }, function (response) {
          var validationErrors = [];
          angular.forEach(response.data, function(value) {
            validationErrors.push(value[0]);
          });
          $scope.validationErrors = validationErrors;
        });
      } else {
        var validationErrors = ['Password do not match'];
        $scope.validationErrors = validationErrors;
      }
  };
});
