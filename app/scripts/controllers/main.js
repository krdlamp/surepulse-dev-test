'use strict';

/**
 * @ngdoc function
 * @name surepulseDevTestApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the surepulseDevTestApp
 */
angular.module('surepulseDevTestApp')
  .controller('MainCtrl', function ($rootScope, $location) {
    if (!$rootScope.authenticated) {
        $location.path('/login');
    }
  })
