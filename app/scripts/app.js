'use strict';

/**
 * @ngdoc overview
 * @name surepulseDevTestApp
 * @description
 * # surepulseDevTestApp
 *
 * Main module of the application.
 */
angular
  .module('surepulseDevTestApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'satellizer'
  ])
  .constant('Config', {
      apiBase: document.domain === 'localhost' ? '//localhost:8000/api' : '//api.surepulse.dev/api',
  })
  .config(function ($routeProvider, $httpProvider, $authProvider, Config) {
    $httpProvider.defaults.useXDomain = true;
    $httpProvider.defaults.paramSerializer = '$httpParamSerializerJQLike';
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $authProvider.loginUrl = Config.apiBase + '/authenticate';
    $authProvider.signupUrl = Config.apiBase + '/register';
    $authProvider.httpInterceptor = true;
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/register', {
        templateUrl: 'views/register.html',
        controller: 'RegisterCtrl'
      })
      .when('/login', {
        templateUrl: 'views/auth.html',
        controller: 'AuthCtrl',
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .run(function ($rootScope) {
      var user = window.localStorage.getItem('user');
      if (user) {
        $rootScope.currentUser = JSON.parse(user);
        $rootScope.authenticated = true;
      } else {
        $rootScope.currentUser = null;
        $rootScope.authenticated = false;
      }
  });
